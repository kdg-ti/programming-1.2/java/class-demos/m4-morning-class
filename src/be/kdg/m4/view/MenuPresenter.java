package be.kdg.m4.view;

import javafx.application.Platform;
import javafx.scene.image.Image;

public class MenuPresenter {
    //private final SomeModel model;
    private final MenuView view;

    public MenuPresenter(MenuView view) {
        this.view = view;

        addEventHandlers();
    }

    private void addEventHandlers() {
        //view.getExitItem().setOnAction(new ExitApplicationEventhandler());
        view.getExitItem().setOnAction(event -> Platform.exit());
        /*
        view.getOpenItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Hello world!");
            }
        });
        */

        // Impossible: (it's an interface!)
        // EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>();

        // setOnKeyPressed / Released / ...
        view.getUserInput().setOnKeyTyped(keyEvent -> {
            System.out.println("KEY: " + keyEvent.getCharacter());

            // DOESN'T WORK ANYMORE
            /*
            if (keyEvent.getCharacter().equals(
                    keyEvent.getCharacter().toUpperCase())) {
                keyEvent.consume(); // prevent upper case letters from being typed
            }
            */
        });

        view.getCharmander().setOnAction(event -> view.getPokemonView().setImage(new Image("/004Charmander.png")));
        view.getSquirtle().setOnAction(event -> view.getPokemonView().setImage(new Image("/007Squirtle.png")));
    }

    /*private void updateView() {
        // Not needed for this demo
    }*/
}
