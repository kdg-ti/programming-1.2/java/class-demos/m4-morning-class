package be.kdg.m4.view;

import javafx.scene.input.MouseEvent;

public class PaintPresenter {
    private final PaintView view;

    public PaintPresenter(PaintView view) {
        this.view = view;

        addEventHandlers();
        //updateView();
    }

    private void addEventHandlers() {
        /*EventHandler<MouseEvent> handler = (MouseEvent mouseEvent) -> {
            double x = mouseEvent.getX();
            double y = mouseEvent.getY();

            // MANY more methods are available
            // mouseEvent.getButton()

            view.getStatusLabel().setText(
                    String.format("X: %5.2f | Y: %5.2f", x, y));
        };*/


        view.getCanvas().setOnMouseMoved((MouseEvent mouseEvent) ->
                updateStatusLabel(mouseEvent.getX(), mouseEvent.getY())
        );

        view.getCanvas().setOnMouseDragged((MouseEvent mouseEvent) -> {
            double x = mouseEvent.getX();
            double y = mouseEvent.getY();

            // MANY more methods are available
            // mouseEvent.getButton()

            // Actual painting
            view.drawDot(x, y);

            updateStatusLabel(x, y);
        });
    }

    private void updateStatusLabel(double x, double y) {
        view.getStatusLabel().setText(
                String.format("X: %5.2f | Y: %5.2f", x, y));
    }

    /*private void updateView() {

    }*/
}
